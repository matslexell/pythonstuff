import re

def order(words):
  return ' '.join(sorted(words.split(), key=lambda w:sorted(w)))
  
def trySorted(words):
	return sorted(words.split(),key=lambda s: re.sub("\D", "", s))
	
#print(int(filter(str.isdigit,"lol")))
	
#print(trySorted("is2 Thi1s T4est 3a"))

def add_binary(a,b):
	return "{0:b}".format((int(a,2) + int(b,2)))
	
def test():
	 return [a*b for a,b in zip(range(1,6), [2]*5)]

	 
spoken    = lambda greeting: greeting + "."
shouted   = lambda greeting: greeting.upper() + "!" 
whispered = lambda greeting: greeting.lower() + "." 
	 
	 
def find_it(seq):
	for x in seq:
		if seq.count(x) % 2 != 0:
			return x
	
def v_sub(string):
	return sorted(string, key=lambda x: chr(ord('9')+1) if x == '0' else '0' if x == '-' else x)
	
def vampire_test(a,b):
	return ''.join(v_sub(str(a*b))) == ''.join(v_sub(str(a)+str(b)))  
	
def disemvowel(string):
    return ''.join()

vowel = ["a","e","y","u","i","o"]
#print(find_it([20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5]))
print(list(filter(lambda x: x in vowel, "This website is for losers LOL!")))